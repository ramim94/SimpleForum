<?php
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Threads.php';
session_start();


$threads= new Threads($connect);

$threads->prepareData($_POST);
$oldpicData=$threads->view();

if(!empty($_FILES["thread_images"]["name"]) || !isset($_FILES["thread_images"]["name"])){
    //update the picture

    $filename= time().$_FILES['thread_images']['name'];
    $location=$_FILES['thread_images']['tmp_name'];
    move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/thread_images/".$filename);

    $_POST['user_id'] = $_SESSION['user_id'];
    $_POST['thread_images']=$filename;

    $threads->prepareData($_POST);
    unlink($_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/thread_images/".$oldpicData['thread_images']);

    if($threads->update()){
        $_SESSION['message']="Thread Updated";
        header("location:logged_home.php");
    }else{
        $_SESSION['message']="Updating Failed";
        header("location:logged_home.php");
    }

}else{
    //only update text informations
    if($threads->update()){
        $_SESSION['message']="Thread Updated";
        header("location:logged_home.php");
    }else{
        $_SESSION['message']="Updating Failed";
        header("location:logged_home.php");
    }

}
