<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create Thread</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create Thread</h2>
    <form role="form" enctype="multipart/form-data" method="post" action="create_thread.php">
        <div class="form-group">
            <label for="thread_title">Title:</label>
            <input type="text" name="thread_title" class="form-control" placeholder="Enter Thread Title">
        </div>
        <div class="form-group">
            <label for="topic">Topic:</label>
            <input type="text" class="form-control" name="thread_topic" placeholder="Enter Topic of the thread">
        </div>
        <div class="form-group">
            <label for="body">Body:</label>
            <textarea class="form-control" name="thread_body" placeholder="Enter Texts"></textarea>
        </div>

        <div class="form-group">
            <label for="image">Image:</label>
            <input type="file" class="form-control" name="thread_images" >
        </div>

        <input name="create_thread" type="submit" class="btn btn-default"/>
        
        <a role="button" class="btn btn-info" href="logged_home.php">Home</a>
    </form>
</div>

</body>
</html>
