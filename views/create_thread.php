<?php
session_start();
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Threads.php';

$threads=new Threads($connect);

if(isset($_POST)) {

    if (isset($_FILES) && !empty($_FILES['thread_images']['name'])) {

        $filename= time().$_FILES['thread_images']['name'];

        $location=$_FILES['thread_images']['tmp_name'];
        move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/thread_images/".$filename);

        $_POST['user_id'] = $_SESSION['user_id'];
        $_POST['thread_images']=$filename;
        
        $threads->prepareData($_POST);
        $ret=$threads->store();

        if ($ret==1) {
            $_SESSION['message'] = "Thread Posted Successfully";
            header("location:logged_home.php");
        } else {
            $_SESSION['message'] = "Thread Posting Failed";
            header("location:logged_home.php");
          }
    }
}