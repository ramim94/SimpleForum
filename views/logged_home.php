<?php
error_reporting(0);
session_start();
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Threads.php';

$threads=new Threads($connect);

if(!isset($_SESSION['user_id']) || is_null($_SESSION['user_id'])){
    //to check login
    header("location:../index.php");
}

if(strtoupper($_SERVER['REQUEST_METHOD'])=="POST"){
    $threads->prepareData($_POST);
}

$alldata= $threads->index();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Simple Forum</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All Threads</h2>

    <?php if(!empty($_SESSION['message']) && !is_null($_SESSION['message'])){ ?>
        <div class="alert alert-success alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $_SESSION['message'];
                $_SESSION['message']="";
            ?>

        </div>
    <?php }?>

    <div class="row">
        <div class="col-sm-9">
            <a role="button" class="btn btn-info" href="addthread.php">Post Thread</a>

            <form method="post" action="logged_home.php">
                <input type="text" class="form-control" name="search_text" placeholder="Search">

                <select class="form-control" name="filter">
                    <option <?php if(isset($_POST['filter'])&&$_POST['filter']=="thread_title") echo "selected"?> value="thread_title">Thread Title</option>
                    <option <?php if(isset($_POST['filter'])&&$_POST['filter']=="thread_body") echo "selected"?> value="thread_body">Thread Body</option>
                    <option <?php if(isset($_POST['filter'])&&$_POST['filter']=="user_name") echo "selected"?> value="user_name">Poster Name</option>
                </select>

                <input type="submit" class="btn-default">
            </form>

            <br>
            <br>

        </div>


        <div class="col-sm-3">

           <a name="logout" class="btn btn-info" href="logout.php">LogOut</a>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Title</th>
            <th>Posted By</th>
            <th>Updated On</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
<?php foreach ($alldata as $data){?>
        <tr>
            <td> <?php echo $data["thread_title"]?>  </td>
            <td> <?php echo $data["user_name"]?>  </td>
            <td> <?php echo $data["thread_date_updated"]?> </td>

            <td><a href="view_thread.php?thread_id=<?php echo $data["thread_id"] ?>" class="btn btn-primary" role="button">View</a>
                <?php if($data['user_id']==$_SESSION['user_id']){ ?>
                <a href= edit_thread.php?thread_id=<?php echo $data["thread_id"] ?> role="button" class="btn btn-success">Edit</a>
                <a href="delete_thread.php?thread_id=<?php echo $data["thread_id"] ?>" role="button" class="btn btn-danger">Delete</a>
                <?php } ?>
            </td>
        </tr>
        <?php }?>
        </tbody>
    </table>


</body>
</html>