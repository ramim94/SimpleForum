<?php

include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Threads.php';
include_once '../src/SimpleForum/Comment.php';
session_start();

$thread= new Threads($connect);
$thread->prepareData($_GET);
$oneThread=$thread->view();

$comments=new Comment($connect);
$comments->prepareData($_GET);
$allComments=$comments->index();
/*
if(strtoupper($_SERVER['REQUEST_METHOD'])=="GET"){
    if(array_key_exists('comment_id',$_GET)){
        var_dump($_GET);die();
    }
}
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single Thread</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>


<div class="container">

<!--    Show Message-->
    <?php if(!empty($_SESSION['message']) && !is_null($_SESSION['message'])){ ?>
        <div class="alert alert-success alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $_SESSION['message'];
            $_SESSION['message']="";
            ?>

        </div>
    <?php }?>

<!--    message done-->
    <div class="row">
        <h2 class="col-sm-9">Single Thread</h2>

        <?php if(!isset($_SESSION['user_id']) || is_null($_SESSION['user_id'])){?>
            <a href="../index.php" class="col-sm-3 btn btn-success" role="button">Home</a>
        <?php }else{ ?>
            <a href="logged_home.php" class="col-sm-3 btn btn-success" role="button">Home</a>
        <?php }?>

    </div>
    <ul class="list-group">
        <li class="list-group-item"> <strong>Thread Title:</strong> <?php echo $oneThread["thread_title"]?> </li>
        <li class="list-group-item"> <strong>Thread Image:</strong> <img src="../images/thread_images/<?php echo $oneThread["thread_images"]?>"/>   </li>
        <li class="list-group-item"> <strong>Thread Body:</strong> <?php echo $oneThread["thread_body"]?> </li>
        <li class="list-group-item"> <strong>Thread Topic:</strong> <?php echo $oneThread["thread_topic"]?> </li>
        <li class="list-group-item"> <strong>Name of Poster:</strong> <?php echo $oneThread["user_name"]?> </li>
        <li class="list-group-item"> <strong>Date Created:</strong> <?php echo $oneThread["thread_date_created"]?> </li>
        <li class="list-group-item"> <strong>Date Updated:</strong> <?php echo $oneThread["thread_date_updated"]?> </li>
    </ul>


<!--    Comment Show section-->

    <div class="container">
        <?php foreach ($allComments as $comment){?>
        <div class="panel panel-primary">
            <div class="panel-heading">Comment</div>
            <div class="panel-body">
    <table class="table table-striped">

        <tr>
            <td><Strong>Comment Body:</Strong></td>
        </tr>
            <tr>
                <td> <?php echo $comment['comment_body']?> </td>
            </tr>
        <?php if(!empty($comment['comment_image'])){?>
        <tr>
            <td><Strong>Comment Image:</Strong></td>
        </tr>
            <tr>
                <td> <img src="../images/comment_images/<?php echo $comment['comment_image']?>" width="200px" height="200px"/></td>
            </tr>
        <?php }?>
        <tr>
            <td><Strong>Commented By:</Strong></td>
        </tr>
            <tr>
                <td><?php echo $comment['user_name']?></td>
            </tr>
        <tr>
            <td><Strong>Updated On:</Strong></td>
        </tr>
            <tr>
                <td><?php echo $comment['comment_date_updated']?></td>
            </tr>
        <tr>
            <td>
                <?php if($comment['user_id']==$_SESSION['user_id']){ ?>

<!--i did it this way because i don't know javascript well, i am skilled in php and android development only-->
<a href= edit_comment.php?comment_id=<?php echo $comment["comment_id"] ?> role="button" class="btn btn-success" >Edit</a>


                    <a href="delete_comment.php?comment_id=<?php echo $comment["comment_id"] ?>" role="button" class="btn btn-danger">Delete</a>
                <?php } ?>
            </td>
        </tr>

    </table>
            </div>
                 </div>
        <?php }?>
    </div>

    <!--    Comment Enter section-->
    <?php if(isset($_SESSION['user_id']) || !is_null($_SESSION['user_id'])){?>
    <form class="form-group well well-sm" enctype="multipart/form-data" method="post" action="create_comment.php">
        <input type="hidden" name="comment_by" value="<?php if(isset($_SESSION['user_id'])){ echo $_SESSION['user_id'];}?>">
        <input type="hidden" name="thread_id" value="<?php echo $oneThread["thread_id"] ?>">
        <textarea class="form-control" placeholder="Enter your comment here" name="comment_body"></textarea>
        <input type="file" class="form-control" name="comment_image">
        <input type="submit" class="form-control btn btn-info">
    </form>
    <?php }?>



</div>

<script >
    function allowInput() {
        document.getElementById("commentBody").readOnly=false;
    }
</script>
</body>
</html>