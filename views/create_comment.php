<?php

session_start();
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Comment.php';

$comment= new Comment($connect);


if(isset($_POST)) {

    if (isset($_FILES) && !empty($_FILES['comment_image']['name'])) {

        $filename= time().$_FILES['comment_image']['name'];

        $location=$_FILES['comment_image']['tmp_name'];
        move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/comment_images/".$filename);

        
        $_POST['comment_image']=$filename;

        $comment->prepareData($_POST);
        
        if ($comment->store()) {
            $_SESSION['message'] = "Comment Posted Successfully";
            header("location:view_thread.php?thread_id=".$_POST['thread_id']);
        } else {
            $_SESSION['message'] = "Comment Posting Failed";
            header("location:view_thread.php?thread_id=".$_POST['thread_id']);
        }
    }else{
        $comment->prepareData($_POST);

        if ($comment->store()) {
            $_SESSION['message'] = "Comment Posted Successfully";
            header("location:view_thread.php?thread_id=".$_POST['thread_id']);
        } else {
            $_SESSION['message'] = "Comment Posting Failed";
            header("location:view_thread.php?thread_id=".$_POST['thread_id']);
        }
    }
}