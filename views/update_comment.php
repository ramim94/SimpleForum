<?php

include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Comment.php';
session_start();

$comments=new Comment($connect);
//var_dump($_POST);
//var_dump($_FILES);
//die();

$comments->prepareData($_POST);

if(!empty($_FILES["comment_image"]["name"]) || !isset($_FILES["comment_image"]["name"])){
    //update the picture

    $filename= time().$_FILES['comment_image']['name'];
    $location=$_FILES['comment_image']['tmp_name'];
    move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/comment_images/".$filename);

    $_POST['user_id'] = $_SESSION['user_id'];
    $_POST['comment_image']=$filename;

    $comments->prepareData($_POST);
    unlink($_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/comment_images/".$oldpicData['comment_image']);

    if($comments->update()){
        $_SESSION['message']="Comment Updated";
        header("location:view_thread.php?thread_id=".$_POST['thread_id']);
    }else{
        $_SESSION['message']="Updating Failed";
        header("location:view_thread.php?thread_id=".$_POST['thread_id']);
    }

}else{
    //only update text informations
    if($comments->update()){
        $_SESSION['message']="Comment Updated";
        header("location:view_thread.php?thread_id=".$_POST['thread_id']);
    }else{
        $_SESSION['message']="Updating Failed";
        header("location:view_thread.php?thread_id=".$_POST['thread_id']);
    }

}
