<?php
error_reporting(0);
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Threads.php';
session_start();

$thread= new Threads($connect);

$thread->prepareData($_GET);

$oneThread=$thread->view();
//var_dump($oneThread);die();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Thread</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Thread</h2>

    <form role="form" enctype="multipart/form-data" method="post" action="update_thread.php">
        <div class="form-group">
            <label for="thread_title">Title:</label>
            <input type="text" name="thread_title" class="form-control" value="<?php echo $oneThread["thread_title"]?>"/>
        </div>
        <div class="form-group">
            <label for="topic">Topic:</label>
            <input type="text" class="form-control" name="thread_topic" value="<?php echo $oneThread["thread_topic"]?>"/>
        </div>
        <div class="form-group">
            <label for="body">Body:</label>
            <textarea class="form-control" name="thread_body"><?php echo $oneThread["thread_body"]?></textarea>
        </div>

        <div class="form-group">
            <label for="image">Image:</label>
            <img src="../images/thread_images/<?php echo $oneThread["thread_images"]?>" />
            <input type="file" class="form-control" name="thread_images" >
        </div>
        
        <input type="hidden" name="thread_id" value="<?php echo $oneThread["thread_id"]?>"> 

        <button name="create_thread" type="submit" class="btn btn-default">Update</button>
        <a href="logged_home.php" class="btn btn-info" role="button">Home</a>
    </form>

</div>


</body>
</html>
