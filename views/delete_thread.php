<?php
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Threads.php';
session_start();


$comments= new Threads($connect);

$comments->prepareData($_GET);
$thisThread= $comments->view();

$_GET['thread_images']=$thisThread['thread_images'];
$comments->prepareData($_GET);

if($comments->delete()){
    $_SESSION['message']="Thread Deleted";
    header("location:logged_home.php");
}
else{
    $_SESSION['message']="Failed to delete the thread";
    header("location:logged_home.php");
}
