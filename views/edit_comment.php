<?php
error_reporting(0);
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Comment.php';
session_start();

$comments=new Comment($connect);
$comments->prepareData($_GET);
$comment=$comments->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single Thread</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="panel panel-primary">
    <div class="panel-heading">Comment</div>
    <div class="panel-body">
        <form action="update_comment.php" method="post" enctype="multipart/form-data">

            <input type="hidden" name="comment_id" value="<?php echo $comment['comment_id']?>" />
            <input type="hidden" name="thread_id" value="<?php echo $comment['thread_id']?>" />
        <table class="table table-striped">
            <tr>
                <td><Strong>Comment Body:</Strong></td>
            </tr>
            <tr>
                <td> <input class="form-control" name="comment_body" value="<?php echo $comment['comment_body']?>"/>  </td>
            </tr>
            <?php if(!empty($comment['comment_image'])){?>
                <tr>
                    <td><Strong>Comment Image:</Strong></td>
                </tr>
                <tr>
                    <td> <img src="../images/comment_images/<?php echo $comment['comment_image']?>" width="200px" height="200px"/></td>
                </tr>
            <?php } ?>
                <tr>
                    <td><input type="file" name="comment_image" class="form-control"></td>
                </tr>

            <tr>
                <td><Strong>Last Updated On:</Strong></td>
            </tr>
            <tr>
                <td><?php echo $comment['comment_date_updated']?></td>
            </tr>
            <tr>
                <td>
                        <input type="submit" role="button" class="btn btn-success" />
                        <a href="view_thread.php?thread_id=<?php echo $comment["thread_id"] ?>" role="button" class="btn btn-danger">Cancel</a>
                </td>
            </tr>
        </table>
        </form>
    </div>
</div>
</body>
</html>