<?php
/**
 * Created by PhpStorm.
 * User: Ramim
 * Date: 2/20/2017
 * Time: 8:11 AM
 */

include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/Comment.php';
session_start();


$comment= new Comment($connect);

$comment->prepareData($_GET);
$thisComment= $comment->view();

$_GET['comment_image']=$thisComment['comment_image'];
$comment->prepareData($_GET);

if($comment->delete()){
    $_SESSION['message']="Comment Deleted";
    header("location:view_thread.php?thread_id=".$thisComment['thread_id']);
}
else{
    $_SESSION['message']="Failed to delete the Comment";
    header("location:view_thread.php?thread_id=".$thisComment['thread_id']);
}
