<?php
include_once '../src/SimpleForum/Connection.php';
include_once '../src/SimpleForum/User.php';
session_start();

$_SESSION['user_id']=null;

session_destroy();
header("location:../index.php");