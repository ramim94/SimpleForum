<?php
/*
 * all account login pass: 1234
 * email: test@gmail.com
 * email: test2@gmail.com
 * email: test3@gmail.com
 * email: test4@gmail.com
 *  */
error_reporting(0);
session_start();
include_once 'src/SimpleForum/Connection.php';
include_once 'src/SimpleForum/User.php';

$user= new User($connect);
$message="";
if(isset($_POST) && !is_null($_POST)){
    if($_POST["submit"]=="Register"){
        $user->prepareData($_POST);
        if($user->create_user()){
            $_SESSION["message"]="Registraion Successful, Please Login";
            header("location:index.php");
        }else{
            $_SESSION["message"]="Registration Failed";
            header("location:index.php");
        }
    }
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Simple Forum Registration</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Register!!!</h2>

    <form role="form" action="" method="post">
        <div class="form-group">
            <label for="userName">Name:</label>
            <input name="user_name" type="text" class="form-control"  placeholder="Enter your name here">
        </div>
        <div class="form-group">
            <label for="userEmail">Email:</label>
            <input name="user_email" type="email" class="form-control"  placeholder="Enter your email here">
        </div>
        <div class="form-group">
            <label for="userPass">Password:</label>
            <input name="user_password" type="password" class="form-control"  placeholder="Enter your password here">
        </div>

        <input name="submit" type="submit" class="btn btn-info" value="Register"/>

        <a href="index.php" class="btn btn-info" role="button">Home</a>
    </form>

</div>

</body>
</html>
