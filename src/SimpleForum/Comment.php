<?php

class Comment{
    public $conn;
    public $comment_id;
    public $thread_id;
    public $comment_by;
    public $comment_body;
    public $comment_image="";
    public $comment_date_created;
    public $comment_date_updated;

    public function __construct($conn)
    {
        $this->conn=$conn;
    }

    public function prepareData($data=array()){
        if(array_key_exists("comment_id",$data)){$this->comment_id=$data["comment_id"];}
        if(array_key_exists("thread_id",$data)){$this->thread_id=$data["thread_id"];}
        if(array_key_exists("comment_by",$data)){$this->comment_by=$data["comment_by"];}
        if(array_key_exists("comment_body",$data)){$this->comment_body=$data["comment_body"];}
        if(array_key_exists("comment_image",$data)){$this->comment_image=$data["comment_image"];}
        if(array_key_exists("comment_date_created",$data)){$this->comment_date_created=$data["comment_date_created"];}
        if(array_key_exists("comment_date_updated",$data)){$this->comment_date_updated=$data["comment_date_updated"];}
    }

    public function store(){
        $this->comment_date_created= date('Y-m-d H:i:s');
        $this->comment_date_updated= date('Y-m-d H:i:s');

        $query="INSERT INTO `comment` (`thread_id`,`comment_by`,`comment_body`,`comment_image`,`comment_date_created`,`comment_date_updated`) VALUES ('".$this->thread_id."','".$this->comment_by."','".$this->comment_body."','".$this->comment_image."','".$this->comment_date_created."','".$this->comment_date_updated."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            return true;
        }else return false;
    }

    public function index(){
        $allinfo=array();
        $query="SELECT `comment_id`,`comment_body`,`comment_image`,`comment_date_updated`, user.user_name, user.user_id FROM `comment` INNER JOIN user WHERE `comment_by`=user.user_id AND thread_id=".$this->thread_id;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $allinfo[]=$row;
        }
        return $allinfo;
    }
    public function view(){
        $query="Select * from `comment` where comment_id=".$this->comment_id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    
    //change individually
    public function update(){
        $this->comment_date_updated= date('Y-m-d H:i:s');
        
        if(!empty($this->comment_image) || !is_null($this->comment_image)){
            $query="UPDATE `comment` SET `comment_body` ='".$this->comment_body."', `comment_image` = '".$this->comment_image."' , `comment_date_updated`='".$this->comment_date_updated."' WHERE `comment_id` =".$this->comment_id;
        }else{
            $query="UPDATE `comment` SET `comment_body` = '".$this->comment_body."', `comment_date_updated`='".$this->comment_date_updated."' WHERE `comment_id` =".$this->comment_id;
        }

      //  var_dump($query);
       // die();

        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }

    public function delete(){
        $query="DELETE FROM `comment` WHERE `comment_id` =".$this->comment_id;
        if(mysqli_query($this->conn, $query)){
            unlink($_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/comment_images/".$this->comment_image);
            return true;
        }else return false;
    }
}