<?php

class Threads{
    public $conn;
    public $thread_id;
    public $user_id;
    public $thread_title;
    public $thread_body;
    public $thread_images;
    public $thread_topic;
    public $thread_date_created;
    public $thread_date_updated;

    public $search_text;
    public $filter;

    public function __construct($conn)
    {
        $this->conn=$conn;
    }
    
    public function prepareData($data=array()){
        if(array_key_exists("thread_id",$data)){$this->thread_id=$data['thread_id'];}
        if(array_key_exists("user_id",$data)){$this->user_id=$data['user_id'];}
        if(array_key_exists("thread_title",$data)){$this->thread_title=$data['thread_title'];}
        if(array_key_exists("thread_body",$data)){$this->thread_body=$data['thread_body'];}
        if(array_key_exists("thread_images",$data)){$this->thread_images=$data['thread_images'];}
        if(array_key_exists("thread_topic",$data)){$this->thread_topic=$data['thread_topic'];}
        if(array_key_exists("thread_date_created",$data)){$this->thread_date_created=$data['thread_date_created'];}
        if(array_key_exists("thread_date_updated",$data)){$this->thread_date_updated=$data['thread_date_updated'];}
        if(array_key_exists("search_text",$data)){$this->search_text=$data['search_text'];}
        if(array_key_exists("filter",$data)){$this->filter=$data['filter'];}
    }

    public function store(){
        $this->thread_date_created= date('Y-m-d H:i:s');
        $this->thread_date_updated= date('Y-m-d H:i:s');


        $query="INSERT INTO `threads` (`user_id`,`thread_title`,`thread_body`,`thread_images`,`thread_topic`,`thread_date_created`,`thread_date_updated`) VALUES ('".$this->user_id."','".$this->thread_title."','".$this->thread_body."','".$this->thread_images."','".$this->thread_topic."','".$this->thread_date_created."','".$this->thread_date_updated."')";
       // var_dump($query);die();
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return mysqli_error($this->conn);
    }
    
    
    public function index(){
        $allinfo=array();

        $whereClause= " 1=1 ";
/*        if(!empty($this->filter)){
            $whereClause.=" AND title LIKE '%".$this->filterTitle."%'";
        }*/
        if(!empty($this->search_text)){
            $whereClause.=" AND `".$this->filter."` LIKE '%".$this->search_text."%'";
        }


        $query="SELECT threads.thread_id,threads.thread_title, threads.user_id, user.user_name, threads.thread_date_updated FROM `threads` INNER JOIN user WHERE user.user_id=threads.user_id AND ".$whereClause;
        
       // var_dump($query); die();
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $allinfo[]=$row;
        }
        return $allinfo;
    }
    public function view(){
        $query="Select `thread_id`, `thread_title`,`thread_body`,`thread_topic`,`thread_images`,`thread_date_created`,`thread_date_updated`,`user_name` from `threads` INNER JOIN user WHERE threads.user_id=user.user_id AND threads.thread_id=".$this->thread_id;
       // var_dump($query);die();
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    
    //change individually
    public function update(){
        $this->thread_date_updated= date('Y-m-d H:i:s');

        if(!empty($this->thread_images) && !is_null($this->thread_images)){
$query="UPDATE `threads` SET `thread_title` = '".$this->thread_title."', `thread_body` = '".$this->thread_body."', `thread_topic` ='".$this->thread_topic."' , `thread_images`='".$this->thread_images."' , `thread_date_updated`='".$this->thread_date_updated."' WHERE `threads`.`thread_id` =".$this->thread_id;
        }else{
            $query="UPDATE `threads` SET `thread_title` = '".$this->thread_title."', `thread_body` = '".$this->thread_body."', `thread_topic` = '".$this->thread_topic."', `thread_date_updated`='".$this->thread_date_updated."' WHERE `threads`.`thread_id` =".$this->thread_id;
        }
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }
    
    public function delete(){
        $comment_delete="DELETE FROM `comment` WHERE `thread_id`=".$this->thread_id;

        if(mysqli_query($this->conn, $comment_delete)){
            $query="DELETE FROM `threads` WHERE `thread_id`=".$this->thread_id;
            if(mysqli_query($this->conn, $query)){
                unlink($_SERVER['DOCUMENT_ROOT']."/SimpleForum/images/thread_images/".$this->thread_images);
                return true;
            }else return false;
        }else return false;
    }

}