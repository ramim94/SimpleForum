<?php

Class User{
    public $conn;
    public $user_id;
    public $user_name;
    public $user_email;
    public $user_password;

    
    public function __construct($conn)
    {
        $this->conn=$conn;
    }

    public function prepareData($data=array()){
        if(array_key_exists('user_id',$data)){$this->user_id=$data["user_id"];}
        if(array_key_exists('user_email',$data)){$this->user_email=$data['user_email'];}
        if(array_key_exists('user_password',$data)){$this->user_password=$data['user_password'];}
        if(array_key_exists('user_name',$data)){$this->user_name=$data['user_name'];}
    }


    public function create_user(){
        $encrypted_password=password_hash($this->user_password,PASSWORD_BCRYPT);

        $sf_create_user="INSERT INTO `user` (`user_email`, `user_password`, `user_name`) VALUES ('".$this->user_email."', '".$encrypted_password."', '".$this->user_name."');
        ";

        if(mysqli_query($this->conn,$sf_create_user))return true;
        else return false;
    }

    public function verify_user(){
       
        $sf_verify_user="Select * from `user` WHERE `user_email` ='".$this->user_email."'";
        $result=mysqli_query($this->conn,$sf_verify_user);
        // var_dump();die();
        if(mysqli_num_rows($result)<=0){
            return false; //no such email
        }else{
            $row=mysqli_fetch_assoc($result);
            if(password_verify($this->user_password,$row['user_password'])){
             return $row;   
            }else{
                return false;//password didnt match
            }
        }
    }
    
}