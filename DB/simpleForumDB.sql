-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2017 at 03:00 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simple_forum`
--
CREATE DATABASE IF NOT EXISTS `simple_forum` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `simple_forum`;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `comment_by` int(11) NOT NULL,
  `comment_body` text NOT NULL,
  `comment_image` text NOT NULL,
  `comment_date_created` datetime NOT NULL,
  `comment_date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `thread_id`, `comment_by`, `comment_body`, `comment_image`, `comment_date_created`, `comment_date_updated`) VALUES
(1, 2, 2, 'asdfasdf', '', '2017-02-20 02:44:45', '2017-02-20 02:44:45'),
(3, 2, 1, 'comment from user1', '', '2017-02-20 02:54:58', '2017-02-20 02:54:58'),
(4, 1, 1, 'comment by sm1', '1487583290Koala.jpg', '2017-02-20 02:55:25', '2017-02-20 10:34:50'),
(6, 1, 2, 'comment from 2', '', '2017-02-20 02:56:00', '2017-02-20 02:56:00'),
(8, 2, 2, 'datafsfe', '1487558592Hydrangeas.jpg', '2017-02-20 03:43:12', '2017-02-20 03:43:12'),
(9, 1, 4, 'from test 4', '1487584238Jellyfish.jpg', '2017-02-20 10:50:38', '2017-02-20 10:50:38'),
(10, 2, 4, 'from test 4', '', '2017-02-20 10:50:53', '2017-02-20 10:50:53'),
(12, 8, 4, 'from test 4', '', '2017-02-20 10:51:09', '2017-02-20 10:51:09'),
(15, 8, 2, 'my comment\r\n', '', '2017-02-20 11:13:39', '2017-02-20 11:13:39'),
(17, 2, 3, 'asdfasdf', '', '2017-02-20 11:14:23', '2017-02-20 11:14:23'),
(18, 4, 4, 'comment', '', '2017-02-20 11:17:59', '2017-02-20 11:17:59');

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `thread_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `thread_title` varchar(255) NOT NULL,
  `thread_body` text NOT NULL,
  `thread_images` text NOT NULL,
  `thread_topic` varchar(255) NOT NULL,
  `thread_date_created` datetime NOT NULL,
  `thread_date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`thread_id`, `user_id`, `thread_title`, `thread_body`, `thread_images`, `thread_topic`, `thread_date_created`, `thread_date_updated`) VALUES
(1, 1, 'first thread', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis ac metus non luctus. Quisque ut augue commodo nisi eleifend blandit. In et laoreet sem, ut sagittis tortor. Nulla tincidunt magna arcu, ultricies consequat leo tincidunt sed. In tempus lorem eu sem tincidunt maximus. Aliquam consequat mauris et commodo tincidunt. Phasellus in pellentesque mauris, eget vestibu', '14874878161.jpg', 'lorem', '2017-02-19 08:03:36', '2017-02-19 08:03:36'),
(2, 2, 'testfrom2', 's augue. Ut cursus sem mauris. Nam molestie non massa vel maximus. Nunc imperdiet velit eu mauris finibus, in lobortis est aliquam. Sed libero velit, sollicitudin finibus nisi ut, sagittis gravida mauris. Duis volutpat congue urna, sed mattis ante hendrerit in. Donec tristique malesuada nibh ac egestas. Donec tempus nunc blandit odio dapibus, non fringilla eros vestibulum. Nulla sed metus nibh. Quisque id lectus nec dui maxi', '14875515034.jpg', 'lorem ipsum', '0000-00-00 00:00:00', '2017-02-20 01:45:03'),
(4, 2, 'secondThrd', 'Quisque aliquet, et laoreet tempus, erat orci facilisis tortor, non sollicitudin ante nunc sed diam. Morbi viverra ultrices interdum. Ut aliquam mauris eu odio malesuada, id sollicitudin nunc rhoncus. Donec risus est, vulputate vitae luctus nec, tincidunt vel ex. Curabitur vel neque vel diam viverra scelerisque ut eget lectus.', '14875515173.jpg', 'my topic', '2017-02-19 16:35:51', '2017-02-20 01:45:17'),
(8, 3, 'Thread by user 3', 'es, and more recently with desktop publishing software like Aldus PageMaker includi', '1487584213Chrysanthemum.jpg', 'different topic', '2017-02-20 10:50:13', '2017-02-20 10:50:13'),
(15, 4, 'asdfasdf', 'asdfasdgasdfsdf', '14875863572.jpg', 'asdgfasdf', '2017-02-20 11:25:57', '2017-02-20 11:25:57');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_password`) VALUES
(1, 'test user 1', 'test@gmail', '$2y$10$E.IZtmjY2nBwe6mkj.eiOOoGMmEpgMpE2gw8nEGwQ44xNA7WL4gmG'),
(2, 'test user 2', 'test2@gmail', '$2y$10$HULbxDjpQBkF/r92Tz41FOqtkmKcFEO3ct2/nXdCuCupY14cIZVyi'),
(3, 'test user 3', 'test3@gmail', '$2y$10$RMXVHRYFKPU2ALkdZ9Jsvevh0jh5NExG0r9vWJ0eqBht2tGMguxUK'),
(4, 'test user 4', 'test4@gmail', '$2y$10$hQN27X9/muUatahYOKIdT.ltBpad3SGdtxZoKzlEzgXb4G3qhCfAm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`thread_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `thread_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
