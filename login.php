<?php

/*
 * all account login pass: 1234
 * email: test@gmail.com
 * email: test2@gmail.com
 * email: test3@gmail.com
 * email: test4@gmail.com
 *  */
error_reporting(0);
include_once 'src/SimpleForum/Connection.php';
include_once 'src/SimpleForum/User.php';
session_start();
$user=new User($connect);

if(isset($_POST)){
    $user->prepareData($_POST);
    $returnData=$user->verify_user();
    if(is_bool($returnData)){
        //wrong info
        $_SESSION['message']="Wrong email or password";
        header("location:index.php");
    }else{
        $_SESSION['user_id']=$returnData['user_id'];
        $_SESSION['message']="Login Successful";
        header("location:views/logged_home.php");
    }
}